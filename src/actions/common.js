import axios from 'axios'
import types from '../constants/actions-types'

export const fetch = (users) => async (dispatch, getState) => {
  try {
    dispatch({ type: types.FETCH, data: true })
    const { data } = await axios.post('/similar-games', { users })
    const { games } = data
    dispatch({ type: types.UPDATE_GAMES, data: games })
    dispatch({ type: types.FETCH, data: false })
  } catch(e){
    console.log(e)
  }
}
