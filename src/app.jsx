import React, { PureComponent, Fragment } from 'react'
import { Provider } from 'react-redux'
import store from './store'

import AppBar from './components/app-bar'
import Form from './components/form'
import Games from './components/games'

import './app.css'

export class App extends PureComponent {
  render() {
    return <Provider store={store}>
      <Fragment>
        <AppBar />
        <Form />
        <Games />
      </Fragment>
    </Provider>
  }
}

export default App
