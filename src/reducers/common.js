import {
  Map as iMap
} from 'immutable'
import types from '../constants/actions-types'

export default (state = iMap({ fetching: false, games: [] }), action) => {
  switch (action.type) {
    case types.FETCH: {
      return state.set('fetching', action.data)
    }
    case types.UPDATE_GAMES: {
      return state.set('games', action.data)
    }

    default: {
      return state
    }
  }
}
