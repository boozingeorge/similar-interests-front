import { combineReducers } from 'redux-immutable'

import { default as commonReducer } from './common'

const rootReducer = combineReducers({
  common: commonReducer
});

export default rootReducer;
