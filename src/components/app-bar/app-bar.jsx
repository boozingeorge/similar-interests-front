import React from 'react'
import { connect } from 'react-redux'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import LinearProgress from '@material-ui/core/LinearProgress'

const styles = () => ({
  progress: {
    position: 'absolute',
    top: 0,
    width: '100%',
  }
})

const mapStateToProps = state => ({
  fetching: state.get('common').get('fetching')
})

const mapDispatchToProps = {
  fetch
}

const ButtonAppBar = ({ classes, fetching }) => {
  return (
    <div>
     { fetching && <LinearProgress className={classes.progress} color="secondary"/>}
      <AppBar position="static">
        <Toolbar>
          <Typography variant="title" color="inherit">
            Similar Interests
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ButtonAppBar))
