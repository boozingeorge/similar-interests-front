import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import some from 'lodash.some'
import { Formik, Field, Form, FieldArray } from 'formik'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Input from '@material-ui/core/Input'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import AddIcon from '@material-ui/icons/Add'
import SendIcon from '@material-ui/icons/Send'
import DeleteIcon from '@material-ui/icons/Delete'

import { fetch } from '../../actions/common'

const styles = theme => ({
  root: {
    margin: '10px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  actions: {
    textAlign: 'center'
  },
  error: {
    color: theme.palette.error.main
  }
})

const initialValues = {
  users: [''],
}

const mapStateToProps = state => ({
  disabled: state.get('common').get('fetching')
})

const mapDispatchToProps = {
  fetch
}


class UserForm extends PureComponent {
  static propTypes = {
    disabled: PropTypes.bool.isRequired,
  }
  static defaultProps = {
    disabled: false,
  }

  render() {
    const {classes, disabled} = this.props
    return <Paper className={classes.root}>
      <div>
        <Formik
          initialValues={initialValues}
          onSubmit={({users}, form) => {
            const invalid = some(users, (value) => value === '')
            if (invalid) return form.setStatus('Все поля должны заполнены')
            form.setStatus(null)
            this.props.fetch(users)
          }}
          render={({ values, errors, touched, status }) => (
            <Form>
              <FieldArray
                name="users"
                render={({ insert, remove, push }) => (
                  <div>
                    {values.users.length > 0 &&
                      values.users.map((user, index) => (
                        <div className="row" key={index}>
                            <Field
                              name={`users.${index}`}
                              type="text"
                              placeholder="Nickname"
                              render={({ field, props }) => <Input {...field}/>}
                            />
                          <IconButton onClick={() => remove(index)} color="primary" disabled={!(values.users.length > 1) || disabled}><DeleteIcon /></IconButton>
                        </div>
                      ))}
                      {status && <Typography className={classes.error}>{status}</Typography>}
                      <div className={classes.actions}>
                        <IconButton onClick={() => push('')} color="primary" disabled={disabled}>
                          <AddIcon />
                        </IconButton>
                        <IconButton type="submit" color="primary" disabled={disabled}>
                          <SendIcon />
                        </IconButton>
                      </div>
                  </div>
                )}
              />
            </Form>
          )}
        />
      </div>
    </Paper>
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(UserForm))
