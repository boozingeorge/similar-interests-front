import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'


const styles = theme => ({
  root: {
    margin: '10px',
    padding: '10px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  game: {
    margin: '5px 10px',
    padding: '0 10px',
  },
  odd: {
    color: theme.palette.primary.contrastText,
    background: theme.palette.secondary.main,
    margin: console.log(theme.palette)
  },
  even: {
    color: theme.palette.secondary.contrastText,
    background: theme.palette.primary.main,
  },
})

const mapStateToProps = state => ({
  disabled: !state.get('common').get('fetching'),
  games: state.get('common').get('games'),
})

class Games extends PureComponent {
  static propTypes = {
    disabled: PropTypes.bool.isRequired,
    disabled: PropTypes.bool.isRequired,
  }
  static defaultProps = {
    disabled: false,
  }

  render() {
    const {classes, disabled, games} = this.props

    return games.length && disabled && <Paper className={classes.root}>
      {games.map((game, index) => <Typography className={cn(classes.game, {[classes.odd]: !(index % 2), [classes.even]: index % 2})} key={game}>{game}</Typography>)}
    </Paper> || null
  }
}

export default connect(mapStateToProps)(withStyles(styles)(Games))
