import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

import rootReducer from  './reducers'

const Store = (initialState) => {
  const enhancers = [applyMiddleware(thunk)];

  /** @namespace window.__REDUX_DEVTOOLS_EXTENSION__ */
  if (window.__REDUX_DEVTOOLS_EXTENSION__) {
    enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__());
  }

  return createStore(rootReducer, initialState, compose(...enhancers));
};


const store = Store();

export default store;
