const FETCH = 'similar/FETCH'
const UPDATE_GAMES = 'similar/UPDATE_GAMES'

export default { FETCH, UPDATE_GAMES }
